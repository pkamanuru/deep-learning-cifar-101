function [feature_map] =  getFeatureMaps(net)
    caltechPath='./Caltech_101_Training/';
    
    category = dir(caltechPath);
    feature_map_index=1;
    for i = 1: length(category)
        if category(i).name(1) == '.' 
            continue
        else
            fprintf('processing %s\n', category(i).name);
            imageFile = dir(fullfile(caltechPath, category(i).name,'image*'));
            
            lim = length(imageFile);
            
            disp(strcat('No of Images:',num2str(lim)));
            disp(strcat('Feature Maps for Category:',num2str(i)));
            
            for j = 1:lim                
                if imageFile(j).name(1) == '.'
                    continue
                else
                    im = imread(fullfile(caltechPath,category(i).name, imageFile(j).name));
                    res=vl_simplenn(net,im2single(im));
                    feature_map.image(:,feature_map_index)=reshape(res(20).x,[4096,1]);
                    feature_map.label(feature_map_index)=i;
                    feature_map_index = feature_map_index + 1;
                end
            end
        end
    end
    feature_map.label=feature_map.label';
    feature_map.image=feature_map.image';
end
