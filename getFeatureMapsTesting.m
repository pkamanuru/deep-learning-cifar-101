function [testing_feature_map] =  getFeatureMapsTesting(net)

    caltechPath='./Caltech_101_Testing/';
    category = dir(caltechPath);
    test_feature_map_index=1;
    for i = 1: length(category)
        if category(i).name(1) == '.' 
            continue
        else
            fprintf('processing %s\n', category(i).name);
            imageFile = dir(fullfile(caltechPath, category(i).name,'image*'));
            
            lim = length(imageFile);
            
            disp(strcat('No of Test Images:',num2str(lim)));
            disp(strcat('Feature Maps for Category:',num2str(i)));
            
            for j = 1:lim                
                if imageFile(j).name(1) == '.'
                    continue
                else
                    im = imread(fullfile(caltechPath,category(i).name, imageFile(j).name));
                    res=vl_simplenn(net,im2single(im));
                    testing_feature_map.image(:,test_feature_map_index)=reshape(res(20).x,[4096,1]);
                    testing_feature_map.label(test_feature_map_index)=i;
                    test_feature_map_index = test_feature_map_index + 1;
                end
            end
        end
    end
    testing_feature_map.label=testing_feature_map.label';
    testing_feature_map.image=testing_feature_map.image';
end
