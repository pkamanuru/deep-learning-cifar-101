function []  = TrainAndValidate()
    net = load('imagenet-caffe-alex.mat');
    % newNet can be applied directly in vl_simplenn()
    newNet = net;
    for i = 1: numel(net.layers)
        if strcmp(net.layers{1,i}.type,'conv')
            newNet.layers{1,i} = rmfield(newNet.layers{1,i},'weights');
            newNet.layers{1,i}.filters = net.layers{1,i}.weights{1,1};
            newNet.layers{1,i}.biases = net.layers{1,i}.weights{1,2};
        end
    end

    training_feature_maps=getFeatureMaps(newNet);
    testing_feature_maps=getFeatureMapsTesting(newNet);
    training_feature_maps.image=double(training_feature_maps.image);
    training_feature_maps.label=double(training_feature_maps.label);
    
    testing_feature_maps.image=double(testing_feature_maps.image);
    testing_feature_maps.label=double(testing_feature_maps.label);
    
    model=svmtrain(training_feature_maps.label,training_feature_maps.image,'-t 0 -c 1');
    [predicted_label] = svmpredict(training_feature_maps.label, training_feature_maps.image, model, '');
    [predicted_label] = svmpredict(testing_feature_maps.label, testing_feature_maps.image, model, '');
end